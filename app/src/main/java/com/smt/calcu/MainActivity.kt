package com.smt.calcu

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.ActionBar
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.switchmaterial.SwitchMaterial
import com.smt.calcu.enums.SpecialFlags
import com.smt.calcu.models.Calc
import java.lang.ArithmeticException
import java.text.DecimalFormat

class MainActivity : AppCompatActivity() {
    private lateinit var calc: Calc

    private lateinit var errorView: TextView
    private lateinit var memView: TextView

    private lateinit var operationsRecycler: RecyclerView
    private lateinit var viewAdapter: RecyclerView.Adapter<*>
    private lateinit var viewManager: RecyclerView.LayoutManager

    private lateinit var toolbar: ActionBar
    private lateinit var bottomNavigation: BottomNavigationView

    private lateinit var keyboardContainer: ConstraintLayout

    private lateinit var switch: SwitchMaterial

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Bottom navigation
        toolbar = supportActionBar!!
        bottomNavigation = findViewById(R.id.bottomNavigationView)
        bottomNavigation.setOnNavigationItemSelectedListener(onNavigationItemSelectedListener)

        // Start with basic layout
        keyboardContainer = findViewById(R.id.keyboardContainer)
        layoutInflater.inflate(R.layout.basic_layout, keyboardContainer)

        // Hide support action bar
        supportActionBar!!.hide()

        // Initialize calc model
        calc = Calc()

        // Setup switch
        switch = findViewById(R.id.rad_deg_switch)
        switch.setOnCheckedChangeListener { button, isChecked ->
            if (isChecked) {
                calc.handleSpecial(button.id, SpecialFlags.SET_DEGREES)
                switch.text = getString(R.string.deg)
            }
            else {
                calc.handleSpecial(button.id, SpecialFlags.SET_RADIANS)
                switch.text = getString(R.string.rad)
            }

        }

        // Views init
        errorView = findViewById(R.id.errorView)
        memView = findViewById(R.id.mem_display)

        memView.text = DecimalFormat("######.###").format(calc.getMem())

        // Display recycler setup
        viewManager = LinearLayoutManager(this)
        viewAdapter = OperationsAdapter(calc)

        operationsRecycler = findViewById<RecyclerView>(R.id.operationsRecycler).apply {
            setHasFixedSize(true)

            layoutManager = viewManager
            adapter = viewAdapter
        }
    }

    // Bottom navigation button click callback
    private val onNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when(item.itemId) {
            R.id.basic_mode -> {
                keyboardContainer.removeAllViews()
                layoutInflater.inflate(R.layout.basic_layout, keyboardContainer)

                return@OnNavigationItemSelectedListener true
            }
            R.id.advanced_mode -> {
                keyboardContainer.removeAllViews()
                layoutInflater.inflate(R.layout.advanced_layout, keyboardContainer)

                return@OnNavigationItemSelectedListener true
            }
        }

        false
    }

    // Click handler for numbers
    fun clickHandler(view: View) {
        clearError()

        calc.changeDisplay((view as Button).text.toString())

        updateScreen()
        operationsRecycler.scrollToPosition(viewAdapter.itemCount - 1)
    }

    // Click handler for operations
    fun handleOperation(view: View) {
        clearError()

        try {
            calc.handleOperation(view.id)
        }
        catch (e: ArithmeticException) {
            setError(getString(e.message!!.toInt()))
        }

        updateScreen()
        operationsRecycler.scrollToPosition(viewAdapter.itemCount - 1)
    }

    // Click handler for special operations
    fun handleSpecial(view: View) {
        calc.handleSpecial(view.id)

        updateScreen()
        operationsRecycler.scrollToPosition(viewAdapter.itemCount - 1)
    }

    // Update screen
    fun updateScreen() {
        viewAdapter.notifyDataSetChanged()
        memView.text = DecimalFormat("######.###").format(calc.getMem())
    }

    // Auxiliary functions for displaying an error
    fun setError(error: String) {
        val toast = Toast.makeText(applicationContext, error, Toast.LENGTH_SHORT)

        errorView.text = error
        toast.show()
    }

    fun clearError() {
        errorView.text = ""
    }
}