package com.smt.calcu.enums

// Flags for special operations
enum class SpecialFlags {
    NONE, SET_DEGREES, SET_RADIANS
}