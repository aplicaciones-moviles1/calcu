package com.smt.calcu.enums

// Enum to set the working angle mode for the calculator
enum class AngleMode {
    DEGREES, RADIANS
}