package com.smt.calcu

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.smt.calcu.models.Calc
import java.text.DecimalFormat

class OperationsAdapter(private val calcModel: Calc) : RecyclerView.Adapter<OperationsAdapter.ViewHolder>() {
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val textView = itemView.findViewById<TextView>(R.id.operationView)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val operationView = LayoutInflater.from(parent.context)
            .inflate(R.layout.operation_layout, parent, false)

        return ViewHolder(operationView)
    }

    override fun getItemCount() = calcModel.getSize()

    // Add item (number) and format to recycler view
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val number: Double = calcModel.getItem(position)
        holder.textView.text = DecimalFormat("#.########").format(number)
    }
}