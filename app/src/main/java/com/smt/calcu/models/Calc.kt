package com.smt.calcu.models

import com.smt.calcu.R
import com.smt.calcu.enums.AngleMode
import com.smt.calcu.enums.SpecialFlags
import java.lang.NumberFormatException
import kotlin.ArithmeticException
import kotlin.math.*


@OptIn(ExperimentalStdlibApi::class)
class Calc {
    private var display = ""
    private var deque: ArrayDeque<Double> = ArrayDeque()
    private var resultCalled: Boolean = false

    private  var mem: Double = 0.0

    private var angleMode: AngleMode = AngleMode.RADIANS

    init {
        deque.addLast(0.0)
    }

    fun getMem(): Double {
        return mem
    }

    fun changeDisplay(data: String, delete: Boolean=false) {
        /**
         * Changes the internal display of the calculator
         */

        if (resultCalled) {
            resultCalled = false
            addElement(0.0)
        }
        
        if (!delete && validateInput(data)) {
            display += data
        }
        else if (delete) {
            if (display.length > 0) {
                display = display.substring(0, display.length - 1)
            }
        }

        deque[deque.size - 1] = stringToDouble(display)
    }

    private fun stringToDouble(string: String) : Double {
        /**
         * Cast the display string to double
         *
         * @return Double
         */

        var ret = 0.0

        try {
            ret = string.toDouble()
        }
        catch (e: NumberFormatException) {
        }

        return ret
    }

    private fun validateInput(input: String): Boolean {
        /**
         * Performs validation on the input string
         *
         * @return true if passes all validations, false otherwise
         */
        var valid = true

        // decimal point validation
        if (display.contains(".") && input == ".") {
            valid = false
        }

        return valid
    }

    private fun addElement(number: Double) {
        /**
         * Push an element to the internal stack
         */

        display = ""
        deque.addLast(number)
    }

    private fun setElement(number: Double) {
        /**
         * Modifies the contents of the head of the stack
         */

        // TODO: Fix problem after decimal point
        display = number.toString()
        deque[deque.size - 1] = number
    }

    fun getSize() = deque.size
    fun getItem(index: Int) = deque[index]

    fun handleOperation(operation: Int) {
        /**
         * Implements all available operations in the calculator
         *
         * @throws ArithmeticException
         */

        var result = 0.0

        if (deque.size >= 2) {

            // two operands operations
            when (operation) {
                R.id.add -> {
                    result = deque.removeLast() + deque.removeLast()
                    callResult(result)
                }
                R.id.substract -> {
                    var n1 = deque.removeLast()
                    var n2 = deque.removeLast()

                    result = n2 - n1
                    callResult(result)
                }
                R.id.divide -> {
                    var n1 = deque.removeLast()
                    var n2 = deque.removeLast()

                    if (n1 == 0.0) {
                        addElement(n2)
                        resultCalled = true

                        throw ArithmeticException(R.string.div_by_zero.toString())
                    }

                    result = n2 / n1
                    callResult(result)
                }
                R.id.multiply -> {
                    result = deque.removeLast() * deque.removeLast()
                    callResult(result)
                }
                R.id.root_n -> {
                    val n2 = deque.removeLast()
                    val n1 = deque.removeLast()

                    // TODO: same as division
                    if (n1 < 0 || n2 == 0.0) {
                        addElement(0.0)
                        resultCalled = true

                        throw ArithmeticException(R.string.negative_root.toString())
                    }

                    result = n1.pow(1/n2)
                    callResult(result)
                }
                R.id.pow -> {
                    val n2 = deque.removeLast()
                    val n1 = deque.removeLast()

                    // TODO: same as division
                    if (n1 < 0 && (abs(n2) > 0 && abs(n2) < 1)) {
                        addElement(0.0)
                        resultCalled = true

                        throw java.lang.ArithmeticException(R.string.negative_root.toString())
                    }

                    result = n1.pow(n2)
                    callResult(result)
                }
            }
        }
        if (deque.size >= 1) {

            // single operands operations
            when (operation) {
                R.id.sin -> {
                    var n = deque.removeLast()

                    if (angleMode == AngleMode.DEGREES) {
                        n = Math.toRadians(n)
                    }

                    result = sin(n)

                    callResult(result)
                }
                R.id.cos -> {
                    var n = deque.removeLast()

                    if (angleMode == AngleMode.DEGREES) {
                        n = Math.toRadians(n)
                    }

                    result = cos(n)

                    callResult(result)
                }
                R.id.tan -> {
                    var n = deque.removeLast()

                    if (abs(n) == 90.0 || abs(n) == Math.toRadians(90.0)) {
                        addElement(0.0)
                        resultCalled = true

                        throw java.lang.ArithmeticException(R.string.negative_root.toString())
                    }

                    if (angleMode == AngleMode.DEGREES) {
                        n = Math.toRadians(n)
                    }

                    result = tan(n)

                    callResult(result)
                }
                R.id.sqrt -> {
                    val n = deque.removeLast()

                    if (n < 0) {
                        addElement(0.0)
                        resultCalled = true

                        throw ArithmeticException(R.string.negative_root.toString())
                    }

                    result = sqrt(n)
                    callResult(result)
                }
                R.id._10_x -> {
                    result = 10.0.pow(deque.removeLast())
                    callResult(result)
                }
                R.id.inv -> {
                    val n = deque.removeLast()

                    if (n == 0.0) {
                        addElement(0.0)
                        resultCalled = true

                        throw java.lang.ArithmeticException(R.string.div_by_zero.toString())
                    }

                    result = 1 / n
                    callResult(result)
                }
                R.id.change_sign -> {
                    result = -1 * deque.removeLast()

                    callResult(result)
                }
            }
        }
    }

    private fun callResult(result: Double) {
        addElement(result)
        resultCalled = true
    }

    fun handleSpecial(operation: Int, flag: SpecialFlags = SpecialFlags.NONE) {
        /**
         * Implements the special operations on the calculator.
         */

        when (operation) {
            R.id.delete -> {
                changeDisplay("", true)
            }
            R.id.enter -> {
                addElement(0.0)
                resultCalled = false
            }
            R.id.store -> {
                mem = deque[deque.size - 1]
            }
            R.id.recall -> {
                setElement(mem)
            }
            R.id.memory_add -> {
                mem += stringToDouble(display)
            }
            R.id.memory_substract -> {
                mem -= stringToDouble(display)
            }
            R.id.mc -> {
                mem = 0.0
            }
            R.id.rad_deg_switch -> {
                if (flag == SpecialFlags.SET_DEGREES) {
                    angleMode = AngleMode.DEGREES
                }
                else if (flag == SpecialFlags.SET_RADIANS) {
                    angleMode = AngleMode.RADIANS
                }
            }
            R.id.ac -> {
                deque.clear()
                addElement(0.0)
                resultCalled = false
            }
        }
    }
}